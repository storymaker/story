# 4. Arc

## How do I structure my story to include a central question, climax, and resolution?

*Time to complete: 10 minutes*

---

In this lesson you will learn:

 * How to combine central question, climax, and resolution in your story
 * How story arcs apply to news stories
 * How to create an arc without a clear resolution

 ---

In the preceding lessons, you learned about the basic structural components of story: central question, climax, and resolution. You need to organize these elements in your story for maximum impact. This is done with a structure called your story arc, or sometimes called arc of tension. For shorter stories and dispatches, the arc will be simple: organized chronologically, for example.

A powerful story moves from an introduction of a central question to a climax where the central question is answered and then to a resolution where the consequence or effect is revealed. The best stories keep the audience asking “What’s next?” as you progress from one component to the next. Each component of the story should be presented in an order that increases tension. These components, or steps, form an arc.

![](images/image01.png)

This is the classic curve of tension in a powerful story. Stories that follow a path of a gradual rise in tension from a central question to a climax that answers the central question is answered, and arriving at the resolution, provide a satisfying experience for the audience.

This concept comes from fictional story writing but works well for news and journalism. If you intend to produce news stories, you must rely on facts and what material you can source, but you can still tell intriguing stories by applying this concept. The desire to create a compelling story structure should not influence your reporting. Story should serve and enhance your journalism. Your journalism should not serve the story.

The arc for some stories might not reach a final climax and resolution before your deadline. You may need to identify a limited, short-term climax. In some cases, the fact that there is no immediate resolution may itself be a sort of climax. For example: if, despite repeated car accidents, local officials do nothing to make the intersection safer.

If you are working on a story that won’t resolve until after your deadline, you may opt to have a source speculate on a likely outcome or talk about the future in your narration. Make sure to address your central question in your conclusion.

---

## THINGS TO REMEMBER

 * Stories are constructed as arcs to help build tension and keep the audience interested.
 * Your arc may begin with introductory material, before the audience knows the central question.
 * Not all stories will have a final resolution in time for your deadline.

 ---

## QUIZ

### 1. True or false: I will have a strong story as long as I have the story components of central question and climax.

 * True
 * False

### 2. In what order should I structure the components for a strong story arc?

 * As long as I include all the components, my story will be fine.
 * The climax should come first; the other components don’t matter.
 * The story begins by introducing the central question, moves on to the climax, and ends with the resolution.
 * The story begins by introducing the climax, moves on to the central question, and ends with the resolution.

### 3. True or false: I can use introductory material to start the story arc before specifying the central question.

 * True
 * False

### 4. How can I produce a story with an arc if there is no resolution to my story at publication time?

(There may be more than one answer.)

 * It’s OK to make a guess as long as there is a resolution.
 * I don’t need to worry about it.
 * I can use a short-term climax in the larger, ongoing story.
 * I can Invite a source to speculate the likely outcome.

---
