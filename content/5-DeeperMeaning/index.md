# 5. Deeper Meaning

## Powerful stories often illustrate a deeper meaning behind the core narrative. How do I do this in my own stories?

*Time to complete: 10 minutes*

---

In this lesson you will learn:

 * How deeper meaning improves your story
 * How to use deeper meaning in news stories
 * How deeper meaning can influence other elements of your story

---

Powerful stories provide a deeper meaning to the basic events and facts in the narrative. They speak to the universal human experience, the fundamental challenges we all face in life. And they do it in a way that audiences can relate to. It might be about lost love, a moral dilemma, economic struggle, triumph toward a dream, or confronting death. This is what the story is really about and is what gives a story impact. Every good story is built on something deeper than the immediate climax.

When audience members recognize the deeper meaning, they make a stronger personal or emotional connection to the story and are more likely to remember it. It is important to focus on the deeper meaning of your story and ask yourself how it will impact your audience. Ask yourself what broad human themes your story addresses.

Different communities will interpret deeper meaning differently. Be sure to consider your audience carefully when thinking through a specific deeper meaning in the story. If you are unclear about what your story’s deeper meaning will be to the audience, your story may not have the impact you want.

Here are some examples of deeper meaning in a variety of stories:

 * In a war, soldiers are hunting a man. They threaten to burn the whole village if he does not give himself up. Central question: Will the man give himself up? Climax: He gives himself up and knows he will be killed, but the village will be saved. Deeper meaning: Some people put their community before themselves. Another deeper meaning here is: Sometimes the few are sacrificed to save the many.

 * In a presidential race, a politician is considering running against his brother. Central question: Will he challenge his brother? Climax: Yes, he enters the race. Deeper meaning: Sometimes ambition is stronger than loyalty.

 * In a local measles outbreak. Central question: will the disease spread outside the community? Climax: Parents keep all infected children in the community at home until they are all no longer infectious. Deeper meaning: Sometimes we need to act together to solve problems.

Whether the audience sees the choices at the climax as right or wrong, or as wise or foolish, does not matter; the audience recognizes that anyone could make these choices;, there is some universal human experience. It would be difficult to disagree with these sentiments — that is what makes them deeper meanings. Stories and programs without a deeper meaning have little impact on the audience; they don’t prompt discussion and won’t be remembered.

---

## THINGS TO REMEMBER

 * All components of your story should work together to convey a deeper meaning.
 * Deeper meanings are not the same as injecting your own opinions into a story. They frame a story around a broader cultural or societal theme.
 * Deeper meanings may differ across audiences from different social or cultural backgrounds.

 ---

## QUIZ

### 1. Which of these is a deeper meaning about the climax to this story? A politician who is found guilty of corruption fails to get re-elected.

 * Once you lose your credibility, it can be very difficult to regain it.
 * Politicians just look after themselves.
 * The innocent are sometimes punished.

### 2. Which of these is a deeper meaning about the climax to this story? An injured soldier is told he will never walk again. He is confined to a wheelchair and struggles for months until he finally takes his first steps.

 * Doctors are often wrong.
 * God wanted to help him.
 * Determination can bring extraordinary results.

----
