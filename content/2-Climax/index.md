# 2. Climax

*Time to complete: 5 minutes*

---

In this lesson you will learn:

 * The role of the climax in story
 * How a climax can be included in a news story
 * The relationship between a climax and central question

 ---

Good stories introduce us to a central idea, often a conflict or question, and a character. They develop the idea along an arc of tension toward some sort of revelation or confrontation, called the climax. The climax is the emotional peak of the story. In a good story, everything that precedes the climax builds toward the climax.

One common problem with a lot of news coverage is that there is no story structure and no climax. Viewers are not taken through a defined story arc, and no climax is delivered. Information is simply strung together and presented as little more than a list of facts; the story moves from beginning to end without dramatic tension or excitement.

Journalists need to think about structure and climax when reporting stories. In a good story, there will be steps or events that increase tension and entice the audience as the story progresses toward the climax. But the climax is the moment the story’s central question is answered.

It is vital that the climax answers the story’s central question, otherwise the story will seem incomplete, and the audience will be left unsatisfied and may feel cheated. Here are some examples of climaxes that answer a central question:

 * Central question: Will the politician prove his innocence? Climax: He is found guilty of corruption.

 * Central question: Will the hurricane destroy the village? Climax: Only 10 houses were left standing.

 * Central question: Is the celebrity the father of this woman’s child? Climax: DNA test results show he is not the father.

 ---

## THINGS TO REMEMBER

 * The climax should answer your central question.

 * The material you are able to report limits the climax you can present and may alter your central question.

 * Consider the potential of your own voice to answer the climax using narration, if it is not clear from your images, but never create facts that aren’t there.

 ---

## QUIZ

### 1. In these stories about a measles outbreak, which climaxes answer the respective central questions?

(There may be more than one example in this list.)

 * Central question: Will parents bring their children for vaccination against measles? Climax: Fifty percent of parents brought their children for vaccination.

 * Central question: Will any children die as a result of the measles outbreak? Climax: So far, no children have died.

 * Central question: Will the epidemic spread outside of this community? Climax: No, all infected children are kept home until they are no longer infectious.

### 2. In these stories about a demonstration in the city’s main square, which climaxes answer the respective central questions?

(There may be more than one example in this list.)

 * Central question: Will the arrested people be charged or released? Climax: All except one demonstrator was released.

 * Central question: Is the president going to resign? Climax: The president refuses to resign.

 * Central question: Are the police going to break up the demonstration? Climax: The police join the demonstrators.

 * Central question: Who will win control of the square? Climax: The standoff between police and demonstrators continues all night.

### 3. In these stories about the injured football player, which climaxes answer the respective central questions?

(There may be more than one example in this list.)

 * Central question: Will he be fit to play in time for the World Cup? Climax: He just makes it onto the World Cup squad, but does not play a single match.

 * Central question: Will he ever play another game? Climax: He has surgery to repair his knee.

 * Central question: Will the football club remove him from the team? Climax: He retires from playing football.

 * Central question: Will his transfer deal still go ahead? Climax: The other club buys him for half the original fee.

---
