# 3. Resolution

## How do I ensure my story has a clear resolution?

*Time to complete: 5 minutes*

---

In this lesson you will learn:

 * The purpose of a resolution in your story
 * How the resolution follows from the climax
 * How to use resolution in creating news stories

 ---

Stories need a clear structure. They need a beginning, middle, and end. They need a central question, an arc, and a climax. They also need resolution. The resolution of a good story comes after the climax. The resolution is where the audience discovers the consequence or effect of the climax. The climax usually involves a significant decision or event. The resolution is where the audience learns the result of that decision or event.

You need to structure your reporting and your story to provide a resolution. You need to show the impact or outcome of the climax. This could be the outcome of a new policy that places a higher tax on oil. This could be the result of cuts in school funding. This could be the impact of a new bus line reaching a previously underserved community.

Sometimes you will need to make it clear there is no resolution — nothing is changing as a result of the story and the climax. For example, you might do a story about a poorly run hospital in which hospital administrators and local officials promise things will change. Your climax is that promise or some dramatic illustration of the problem. Your resolution might show that, in fact, nothing has changed.

The following examples build on the discussion in Lesson 6.2 Climax.

 * Climax: The politician is found guilty of corruption Resolution: He commits suicide.

 * Climax: Only 10 houses were left standing after the hurricane. Resolution: Everyone leaves the village, and it becomes a ghost town.

 * Climax: DNA results show the celebrity is not the father of the woman’s child. Resolution: He sues her for defamation.

## THINGS TO REMEMBER

 * The resolution explains the consequence or effect of your story’s climax.

 * The resolution should tie the story together, completing the arc that begins with the central question and proceeds to the climax.

 * In a news environment, your resolution may function not to end the story but to introduce the next step of your coverage, thereby introducing your next central question.

## QUIZ

### 1. In these stories about the outbreak of measles in a community, which of these resolutions properly follows its climax?

(There may be more than one example in this list.)

 * Climax: Fifty percent of parents brought their children for vaccination against measles. Resolution: The threat of measles spreading still remains.

 * Climax: All infected children in the community are kept at home until they are all no longer infectious. Resolution: The epidemic does not spread, and the town’s parents become national heroes.

### 2. In these stories about a demonstration in the city’s main square, which of these resolutions properly follows its climax?

(There may be more than one example in this list.)

 * Climax: All except one demonstrator was released. Resolution: The demonstrations continue.

 * Climax: The president refuses to resign. Resolution: She is the second female president of the country.

 * Climax: The police join the demonstrators. Resolution: The army is ordered to replace the police in defending the parliament building.

### 3. In these stories about an injured football player, which of these resolutions properly follows its climax?

(There may be more than one example in this list.)

 * Climax: He just makes it onto the World Cup squad but does not play in a single match. Resolution: His team is disqualified from the World Cup.

 * Climax: He retires from playing football. Resolution: His wife leaves him.

 * Climax: The other club buys him for half the original fee. Resolution: He loses his advertising contracts.

 ---
