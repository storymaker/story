# 6. Symbols

## How can I use symbols in my story?

*Time to complete: 5 minutes*

---

In this lesson you will learn:

 * What makes something a symbol
 * How symbols improve your story
 * How to find a powerful symbol

---

Good stories make use of symbols. A symbol is anything that has powerful associated meaning for people. It can be an object, action, name, or event. Symbols often mean different things to different people, particularly for people from different societies or cultures. To make your stories powerful, you should include visual symbols. Audiences understand them emotionally and quickly. Symbols alone can tell great stories.

One way to tell great stories is through following the changing meaning of symbols. For example, in the first days after the 2011 revolution in Libya, graffiti covered the walls of the nation’s largest cities. At first this graffiti stood as a symbol of hope, of the desires of Libya’s new generation. That same graffiti now stands as a symbol of the lost hope and foolish dreams of Libya’s people.

Sometimes symbols are powerful precisely because they endure, for example, an image of a mountain that remains unchanged after an earthquake or a statue of a great cultural figure from long ago.

Too often news stories portray information in only a literal way. The reporter doesn’t consider how to convey the information or tell a story in a more visually compelling way using symbols. Visual content presented without thought to the power of symbolism can be simplistic. This lack of depth is a primary reason news can seem dull and uninteresting.

It is important not to overuse symbols or use them in ways that are clichéd or can be interpreted as propaganda. It is also important to remember people understand symbols in different ways.

Here are some examples of symbols and how they change as the story develops:

 * In a war, soldiers are hunting a man. The village hides him, but is threatened with destruction if he does not come out. Before he gives up, he symbolizes selfishness; he is willing to let the whole village suffer just for him. When he gives up and saves the village, he symbolizes bravery and generosity.

 * In a presidential race, a politician considers running against his brother. When the politician speaks in favor of his brother, he symbolizes loyalty. When he declares his own candidacy against his brother, he symbolizes betrayal.

 * A football player who scores the cup-winning goal symbolizes a hero. When he is exposed as a an abusive husband, he is seen as a coward.

 ---

## QUIZ

### 1. A politician who is found guilty of corruption fails to get re-elected. He symbolizes:

 * Arrogance
 * The electoral system
 * Punishment

### 2. An injured soldier is told he will never walk again. He is confined to a wheelchair and struggles for months until he finally takes his first steps. He symbolizes:

 * A miracle
 * Bad doctors
 * Determination

---
