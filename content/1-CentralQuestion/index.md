# 1. Central Question

## How do I create news coverage that tells a story?

*Time to complete: 5 minutes*

---

In this lesson you will learn:

 * What makes a central question.
 * How a central question improves your story.
 * How the central question relates to your audience.

 ---

When people read an article or watch a video segment in the news, it needs to provide information relevant to people’s lives. From the outset it needs to be clear to the viewer what information a story is going to provide — what specific question is being asked and what the answer is. This is your central question. If a news segment has no clear central question, or if the segment doesn’t answer the question, the audience will feel unsatisfied or, worse, misled by your story.

The challenge for journalists is that our coverage does not always turn out as we’d expect. Sometimes it is hard to know the central question of your story when your coverage begins, or the question may change as your story evolves. You need to be aware of these uncertainties or shifts and adjust your reporting or presentation to make sure there is a central question and an answer.

Early on, the story must establish a question the audience expects the story will answer. You must always be aware of your central question when reporting and editing your story. The central question is often about emotional outcome rather than about information and facts. Frame your story around the central question, not only around answering “who, what, where, when, why, and how,” the informational elements also known as the Five Ws and H. Facts are a necessary part of news stories, but the facts should be told through story. Asking “What will happen next?” builds story. The story directs the delivery of the facts, not the other way around. A clear initial central question is the starting point that launches a great story.

Here are some examples:

 * In a news story about a court case, the central question might be: “Will the defendant be found guilty or not guilty?” As a news piece, the story must provide a concise overview of the case. If this is the central question, you should frame the story with that question always in mind.

 * In a news story about trouble in a celebrity marriage, the central question might be: “Will they stay together or get divorced?” This central question helps frame the story to look to the future and encourages the audience to keep asking, “What happens next?”

 * The central question for a breaking news story about a traffic accident might be: “How many were injured?” “Will the injured survive?” Or if the person who caused the accident fled, the question might be: “Will the responsible driver be caught?” All of these are strong central questions that help direct and guide the story.

Another way of thinking about the central question is to ask yourself, “What about this story most interests my audience?” “Why does the audience care?” or, “What does the audience most want to know?”

As these examples show, there can be more than one possible central question to pursue. Choosing a single central question helps you focus your reporting & story. This helps you keep your story focused, and your approach. In cases where you have more than one really good central question, you might pursue creating more than one story or creating a series of stories.

---

## THINGS TO REMEMBER

 * The central question may not always be obvious at the beginning of your story.

 * Your central question may change if your story shifts in some unpredicted way. That is how good journalism often works. Make sure your central question shifts with your story.

 * No matter where your central question is revealed, no matter if it changes, you must always have a central question and provide a clear answer.

 ---

## QUIZ

### 1. In a story about an outbreak of measles in a community, which of these central questions might help create a powerful story?

(There may be more than one example in this list.)

 * Will parents bring their children for vaccination against measles?

 * How is measles caused?

 * Why has this epidemic broken out?

 * Who has been affected so far?

 * Which community has been affected?

 * Will any children die in this community as a result of the measles outbreak?

 * Will the epidemic spread outside of this community?

### 2. In a story about a demonstration in the city’s main square, which of these central questions might help create a powerful story?

(There may be more than one example in this list.)

 * How many people were involved in the demonstration?

 * Was anyone arrested?

 * Will the arrested people be charged or released?

 * Why are the people demonstrating?

 * Is the president going to resign?

 * Are the police going to break up the demonstration?

 * Who will win control of the square?

### 3. In a story about an injured football player, which of these central questions might help create a powerful story?

(There may be more than one example in this list.)

 * Will he be fit to play in time for the World Cup?

 * What is the extent of the injury?

 * What will he have to do to get fit again?

 * Will he ever play another game?

 * Will the football club remove him from the team?

 * Will his transfer deal still go ahead?

 * Why are so many footballers getting injured these days?
